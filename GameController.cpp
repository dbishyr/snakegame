#include "GameController.h"

/*
* These defines provide correct keyboard input
* while bouth using English and Russian language
* as well as upper and lower case letters
*/
#define ESC 27
#define R 170
#define RC 138
#define r 114
#define RR 82

/*
* Function that called every tick
*/
void GameController::Tick()
{
	//Validity checks
	if (!m_InputManager)
	{
		return;
	}

	if (!bFruitExists)
	{
		GenerateFruit();
	}

	//Functions that need to be called every tick
	EatCheck();
	MovePlayer();
	GameOverCheck();
	UpdateScreen();
}

/*
*Function that updates main screen information 
*/
void GameController::UpdateScreen()
{
	//Validity checks
	if (!m_Screen)
	{
		return;
	}

	Snake* Player = m_InputManager->GetSnake();
	if (!Player)
	{
		return;
	}
	const int ScreenSize = m_Screen->GetScreenSize();

	for (int y = 0; y < ScreenSize; ++y)
	{
		for (int x = 0; x < ScreenSize; ++x)
		{
			//Updates snake & playing area information
			UpdateGrid(Player, x, y, ScreenSize);
		}
	}

	//Updates fruit information
	if (bFruitExists)
	{
		m_Screen->SetGridValue(m_Fruit->GetY(), m_Fruit->GetX(), m_FruitTile);
	}

	//Updates snake head position
	m_Screen->SetGridValue(Player->GetY(), Player->GetX(), m_SnakeHead);
}

/*
* Generates a new fruit
*/
void GameController::GenerateFruit()
{
	//Validity check
	if (!m_Fruit)
	{
		return;
	}

	//Generating random fruit coordinates
	srand(static_cast<unsigned int>(time(nullptr)));
	int FruitX = (1 + rand() % (m_Screen->GetScreenSize() - 2));
	int FruitY = (1 + rand() % (m_Screen->GetScreenSize() - 2));

	Snake* Player = m_InputManager->GetSnake();

	if (!Player)
	{
		return;
	}

	//Checking the validity of this position for not to spawn
	// on the top of players body
	const int BodySize = static_cast<int>(Player->GetBody().size());
	for (int BodyIndex = 0; BodyIndex < BodySize; ++BodyIndex)
	{
		if (FruitX != Player->GetBody()[BodyIndex].x &&
			FruitY != Player->GetBody()[BodyIndex].y)
		{
			//If valid, set fruit coordinates equal to these 
			m_Fruit->SetX(FruitX);
			m_Fruit->SetY(FruitY);
			bFruitExists = true;
		}
	}
}

/*
* Function that controlls snake eating sequence
*/
void GameController::EatCheck()
{
	Snake* Player = m_InputManager->GetSnake();

	//Validity check
	if (!Player)
	{
		return;
	}

	if (m_Fruit->GetX() == Player->GetX() && m_Fruit->GetY() == Player->GetY())
	{
		//Validity check
		if (!m_Stats)
		{
			return;
		}

		//Updating player stats with the eaten fruits' weight
		m_Stats->SetScore(m_Stats->GetScore() + m_Fruit->GetScoreWeight());

		//Increasing player length
		Player->Grow();

		//Increasing player speed every 5th eaten fruit
		if (static_cast<int>(Player->GetBody().size()) % 5 == 2)
		{
			Player->SetSpeed(Player->GetSpeed() + 1);
			if (Player->GetSpeed() != 0)
			{
				SpeedModifier = m_TickDelay / Player->GetSpeed();
			}
		}
		//Updating player speed in stat bar
		m_Stats->SetSpeed(Player->GetSpeed());
		bFruitExists = false;
	}
}
/*
* This function checks the game over state 
*/
void GameController::GameOverCheck()
{
	//Validity checks
	if (!m_Screen)
	{
		return;
	}

	Snake* Player = m_InputManager->GetSnake();
	if (!Player)
	{
		return;
	}
	const int ScreenSize = m_Screen->GetScreenSize();

	//Checks board border collision
	if (Player->GetX() == ScreenSize - 1 || Player->GetX() == 0 ||
		Player->GetY() == ScreenSize - 1 || Player->GetY() == 0)
	{
		bIsGameOver = true;
	}
	else
	{
		//Check player collision with its own body
		bIsGameOver = Player->Collision();
	}	
}

void GameController::MovePlayer()
{
	//Validity check
	Snake* Player = m_InputManager->GetSnake();
	if (Player)
	{
		Player->Move();
	}
}

/*
*Upadetes screen grid values with snake & general surroundings parameters
*/
void GameController::UpdateGrid(Snake* Player, int x, int y, int ScreenSize)
{
	//Validity check
	if (!Player)
	{
		return;
	}

	//Setting up the border
	if (y == ScreenSize - 1 || x == ScreenSize - 1 || y == 0 || x == 0)
	{
		m_Screen->SetGridValue(y, x, m_Border);
	}
	else
	{
		bool isBody = false;
		const int BodySize = static_cast<int>(Player->GetBody().size());

		for (int BodyIndex = 0; BodyIndex < BodySize; ++BodyIndex)
		{
			if (y == Player->GetBody()[BodyIndex].y && x == Player->GetBody()[BodyIndex].x)
			{
				//Detecting players body parts and setting them to the grid
				isBody = true;
				m_Screen->SetGridValue(y, x, m_SnakeBodyPart);
				break;
			}
		}
		if (!isBody)
		{
			//Setting up empty tiles
			m_Screen->SetGridValue(y, x, m_EptyTile);
		}
	}
}

/*
* Restarts game cycle if called
*/
void GameController::Restart()
{
	system("CLS");
	//Validity checks
	if (!m_Stats)
	{
		return;
	}
	if (!m_InputManager)
	{
		return;
	}
	if (m_InputManager->GetSnake())
	{
		//Deleting old player snake
		delete m_InputManager->GetSnake();
	}

	//Creating new one
	m_InputManager->SetPlayerSnake(new Snake);

	//Setting other variables to their initial values
	m_Stats->SetSpeed(1);
	m_Stats->SetScore(0);
	SpeedModifier = m_TickDelay;
	bFruitExists = false;
	bIsGameOver = false;
}

/*
* Handles the game over sequence
*/
void GameController::GameOverHandle()
{
	while (true)
	{
		//Gets input from keyboard
		int Input = m_InputManager->GetInput();

		//Checks if player wants to restart or exit and performs the corresponding action
		if (Input == ESC)
		{
			bExit = true;
			break;
		}
		if (Input == r || Input == RR ||
			Input == R || Input == RC)
		{
			Restart();
			break;
		}
	}
}
