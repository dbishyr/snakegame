#pragma once

//Sets screen size
#define SCREEN_SIZE 25

/*
* Class that stores main screen information
*/
class Screen
{
public:
	
	//Gets and set for working with private variable
	int GetScreenSize() const { return SCREEN_SIZE; }
	char GetGridValue(int x, int y) const { return Grid[y][x]; }
	void SetGridValue(int x, int y, char ch) { Grid[y][x] = ch; }

private:

	//Main grid
	char Grid[SCREEN_SIZE][SCREEN_SIZE];
};

