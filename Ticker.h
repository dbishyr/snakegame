#pragma once

#include<vector>
#include<ctime>

#include"ITickable.h"

/*
* Responds for game cycle, calls Tick() method
* of every added tickable class
*/
class Ticker
{
public:

	//Called every tick
	void Tick();

	//Addes tickable class to viewport
	void AddTickable(ITickable* tickable);

	//Sets game tick delay
	void SetTickDelay(float delay) { TickDelay = delay; }

private:

	//Time variable
	float Time = static_cast<float>(clock());

	float TickDelay = 0.1f;

	//Stores all tickable classes references 
	std::vector<ITickable*> Tickables;
};

