#pragma once

#include<vector>

/*
*Enum class that defines move directions & invalid one  
*/
enum class Direction
{
	None,

	UP,
	DOWN,
	RIGHT,
	LEFT
};

/*
* Structure for defining coordinates
*/
struct Coord
{
	int x;
	int y;
};

/*
* Class that stores all the information about players' snake
*/
class Snake
{
public:

	//Gets and sets for getting access to private variables
	int GetX() const { return PosX; }
	int GetY() const { return PosY; }
	int GetSpeed() const { return Speed; }
	std::vector<Coord> GetBody() const { return Body; }

	void SetX(int x) { PosX = x; }
	void SetY(int y) { PosY = y; }
	void SetSpeed(int speed) { Speed = speed; }

	//Function to change moving dirrection
	void ChangeDirection(Direction dir);

	void Move();
	void Grow();

	//Checking for collision with self
	bool Collision();

private:

	//Checking for opposite dirrecton tto make
	//player impossible to move from up to down instantly, for example
	bool IsOpposite(Direction dir) const;

	//Stores snake body
	std::vector<Coord> Body;

	//Stores snale moving direction
	Direction Dir = Direction::DOWN;

	//Other stat variables, initialized with base values
	int PosX = 5;
	int PosY = 5;
	int Length = 2;
	int Velocity = 1;
	int Speed = 1;
};

