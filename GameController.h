#pragma once
#include<ctime>

#include"Screen.h"
#include"ITickable.h"
#include"InputManager.h"
#include"Fruit.h"
#include"StatWidget.h"

/*
*Class that responds for all ingame cycle logic 
*/
class GameController : public ITickable
{
public:

	//Called every tick
	void Tick() override;

	//sets the member input manager class
	void SetInputManager(InputManager* input) { m_InputManager = input; }

	//Restarts the game
	void Restart();

	//Handles game over sequence
	void GameOverHandle();

	//Gets for returning private fields of the class
	Screen* GetScreen() const { return m_Screen; }
	StatWidget* GetStats() const { return m_Stats; }
	float GetSpeedMod() const { return SpeedModifier; }

	bool GetIsGameOver() const { return bIsGameOver; }
	bool PendingExit() const { return bExit; }

private:

	//Updates screen information
	void UpdateScreen();

	//Generates fruit on random position
	void GenerateFruit();

	//Handles eating action
	void EatCheck();

	//Checks for game over
	void GameOverCheck();

	//Moves player
	void MovePlayer();

	//Updates screen grid with snake & board information
	void UpdateGrid(Snake* Player, int x, int y, int ScreenSize);

	//Member classes
	Screen* m_Screen = new Screen;
	Fruit* m_Fruit = new Fruit;
	StatWidget* m_Stats = new StatWidget;

	InputManager* m_InputManager = nullptr;
	
	//Boolean variables
	bool bExit = false;
	bool bIsGameOver = false;
	bool bFruitExists = false;

	//Symbols that going to be drawn at corresponding tiles
	const char m_Border        = '#';
	const char m_SnakeBodyPart = 'o';
	const char m_SnakeHead     = 'O';
	const char m_FruitTile	   = '$';
	const char m_EptyTile      = ' ';	

	//Game speed & tick modifiers
	const float m_TickDelay = 0.1f;
	float SpeedModifier = 0.1f;
};

