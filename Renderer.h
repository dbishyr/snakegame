#pragma once
#include"ITickable.h"
#include"GameController.h"

/*
* Responds for outputing game values to the players' screen
*/
class Renderer : public ITickable
{
public:

	//Called every tick
	virtual void Tick() override;

	//Sets game controller member class
	void SetController(GameController* controller) { Controller = controller; }

	//Draws main menu
	void DrawMainMenu() const;

private:

	//Functions to draw other game parts
	void DrawStats() const;
	void DrawScreen() const;
	void DrawDeathScreen() const;

	GameController* Controller = nullptr;
};

