#pragma once

/*
* Interface for tickable classes
* that allows them to call Tick() every frame
*/
class ITickable
{
public:
	virtual void Tick() = 0;
};

