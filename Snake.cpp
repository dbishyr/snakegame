#include "Snake.h"

/*
* Changes snake moving dirrection
*/
void Snake::ChangeDirection(Direction dir)
{
	//Checking if dirrection is valid & non opposite 
	if (dir != Direction::None && !IsOpposite(dir))
	{
		Dir = dir;
	}
}

/*
* Moving the snake
*/
void Snake::Move()
{
	//Changing head position
	switch (Dir)
	{
		case Direction::UP: PosY -= Velocity; break;
		case Direction::DOWN: PosY += Velocity; break;
		case Direction::LEFT: PosX -= Velocity; break;
		case Direction::RIGHT: PosX += Velocity; break;
	}

	//Adding new head position to body
	Body.push_back({ PosX, PosY });

	//Erasing the old one
	if (static_cast<int>(Body.size()) > Length)
	{
		Body.erase(Body.begin());
	}
}

/*
* Increasing snake's length
*/
void Snake::Grow()
{
	//Adding new body part
	Body.push_back({ PosX, PosY });
	Length++;
}

/*
* Checks snake collision with self
*/
bool Snake::Collision()
{
	//Going through all body parts and checking coordinates
	//for equality, except the first 
	const int BodySize = static_cast<int>(Body.size());
	for (int BodyIndex = 0; BodyIndex < BodySize - 1; ++BodyIndex)
	{
		if (PosX == Body[BodyIndex].x &&
			PosY == Body[BodyIndex].y && BodyIndex != 1)
		{
			return true;
		}
	}
	return false;
}

/*
* Checking if new dirrection being opposite to the current one 
*/
bool Snake::IsOpposite(Direction dir) const
{
	if (dir == Direction::DOWN && Dir == Direction::UP ||
		dir == Direction::UP && Dir == Direction::DOWN ||
		dir == Direction::LEFT && Dir == Direction::RIGHT ||
		dir == Direction::RIGHT && Dir == Direction::LEFT)
	{
		return true;
	}
	else
	{
		return false;
	}
}


