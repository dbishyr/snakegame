#include "Ticker.h"

/*
* Called every tick
*/
void Ticker::Tick()
{
	//Substracts system time with recently calculated time 
	//and calling tick if enough time passed 
	if ((static_cast<float>(clock()) - Time) / CLOCKS_PER_SEC >= TickDelay)
	{
		for (const auto Tickable : Tickables)
		{
			//Validity check
			if (Tickable)
			{
				//Calling Tick() of every added tickable class
				Tickable->Tick();
			}
		}
		//Recalculating time for further computing
		Time = static_cast<float>(clock());
	}
}

/*
* Adds tickable classes to viewport
*/
void Ticker::AddTickable(ITickable* tickable)
{
	Tickables.push_back(tickable);	
}
