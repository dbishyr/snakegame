#include <iostream>
#include <windows.h>

#include "Renderer.h"

/*
* Draws stat widget that shows player statistics
*/
void Renderer::DrawStats() const
{
	//Validity checks
	if (!Controller)
	{
		return;
	}

	StatWidget* Stats = Controller->GetStats();

	if (!Stats)
	{
		return;
	}

	//Outputing player stats
	std::cout << "Score: " << Stats->GetScore() << "   Speed: " << Stats->GetSpeed() << std::endl;

}

/*
* Draws main game screen
*/
void Renderer::DrawScreen() const
{
	//Validity checks
	if (!Controller)
	{
		return;
	}

	Screen* Screen_ = Controller->GetScreen();
	if (!Screen_)
	{
		return;
	}

	const int ScreenSize = Screen_->GetScreenSize();
	for (int x = 0; x < ScreenSize; ++x)
	{
		for (int y = 0; y < ScreenSize; ++y)
		{
			//Outputting values for every grid tile, set up by game controller
			std::cout << Screen_->GetGridValue(x,y);
		}
		std::cout << std::endl;
	}
}

/*
* Outputs death screen
*/
void Renderer::DrawDeathScreen() const
{
	system("CLS");

	//Validity checks
	if (!Controller)
	{
		return;
	}

	Screen* Screen_ = Controller->GetScreen();
	if (!Screen_)
	{
		return;
	}

	//Drawing death screen
	const int ScreenSize = Screen_->GetScreenSize();
	for (int x = 0; x < ScreenSize; ++x)
	{
		for (int y = 0; y < ScreenSize; ++y)
		{
			//Typing strings at the middle of the screen, while checking its position 
			// & if it actually fits on a screen 
			if (x == ScreenSize / 2 && y == (ScreenSize - 9) / 2)
			{
				std::cout << "YOU DIED";
				y += 8;
				++x;
			}
			else if (x == ScreenSize / 2 + 3 && y == (ScreenSize - 15) / 2)
			{
				std::cout << "Your score was " << Controller->GetStats()->GetScore();
				y += 12;
				++x;
			}
			else if (x == ScreenSize / 2 + 5 && ScreenSize > 19 && y == (ScreenSize - 19) / 2)
			{
				std::cout << "Press R to restart";
				y += 18;
				++x;
			}
			else if (x == ScreenSize / 2 + 7 && ScreenSize > 19 && y == (ScreenSize - 15) / 2)
			{
				std::cout << "Or ESC to exit";
				y += 14;
				++x;
			}
			//Outputing blank squares for empty tiles
			else std::cout << ' ';
		}
		std::cout << std::endl;
	}

}

/*
* Called every tick
*/
void Renderer::Tick()
{
	//Validity check
	if (!Controller)
	{
		return;
	}
	if (Controller->GetIsGameOver())
	{
		DrawDeathScreen();
		Controller->GameOverHandle();
	}
	DrawStats();
	DrawScreen();

	//Moving cursor to starting position to prevent output lag
	const HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	if (handle)
	{
		SetConsoleCursorPosition(handle, {0,0});
	}
}

/*
* Outputs main menu
*/
void Renderer::DrawMainMenu() const
{
	//Validity checks
	if (!Controller)
	{
		return;
	}

	Screen* Screen_ = Controller->GetScreen();
	if (!Screen_)
	{
		return;
	}

	//Drawing main menu
	const int ScreenSize = Screen_->GetScreenSize();
	for (int x = 0; x < ScreenSize; ++x)
	{
		for (int y = 0; y < ScreenSize; ++y)
		{
			//Drawing borders
			if (x == 0 || y == 0 || y == (ScreenSize - 1) || x == (ScreenSize - 1))
			{
				std::cout << '*';
			}

			//Typing strings at the middle of the screen, while checking its position 
			// & if it actually fits on a screen 
			else if (x == ScreenSize / 2 && ScreenSize > 13 && y == (ScreenSize - 14) / 2)
			{
				std::cout << "C++ Snake Game";
				y += 13;
				++x;
				
			}
			else if (x == ScreenSize / 2 + 3 && ScreenSize > 12 && y == (ScreenSize - 13) / 2)
			{
				std::cout << "Press any key";
				y += 12;
				++x;
			}
			else if (x == ScreenSize - 4 && ScreenSize > 19 && y == (ScreenSize - 19) / 2)
			{
				std::cout << "Made by Denys Bishyr";
				y += 19;
				++x;
			}
			//Outputing blank squares for empty tiles
			else std::cout << ' ';
		}
		std::cout << std::endl;
	}
}
