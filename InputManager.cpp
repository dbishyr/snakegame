#include "InputManager.h"

#include<conio.h>

/*
* These defines provide correct keyboard input
* while bouth using English, Russian and Ukrainian
* language as well as upper and lower case letters
*/

//wasd
#define INPUT_UP 119
#define INPUT_DOWN 115
#define INPUT_LEFT 97
#define INPUT_RIGHT 100

//Russian alternatives for wasd
#define INPUT_UP_R 230
#define INPUT_DOWN_R 235
#define INPUT_LEFT_R 228
#define INPUT_RIGHT_R 162

//WASD
#define INPUT_UP_C 87
#define INPUT_DOWN_C 83
#define INPUT_LEFT_C 65
#define INPUT_RIGHT_C 68

//Russian capital alternatives for wasd
#define INPUT_UP_RC 150
#define INPUT_DOWN_RC 155
#define INPUT_LEFT_RC 148
#define INPUT_RIGHT_RC 130

//i
#define INPUT_DOWN_U 63

/*
* Called every tick
*/
void InputManager::Tick()
{
	//Check for keyboard hit
	if (!_kbhit())
	{
		return;
	}
	//Validity check
	if (!m_PlayerSnake)
	{
		return;
	}
	//Change direction to corresponding input
	m_PlayerSnake->ChangeDirection(GetInputDirection(_getch()));
}

/*
* Gets players input value
*/
int InputManager::GetInput() const
{
	//Check for keyboard hit
	if (_kbhit)
	{
		return _getch();
	}
	else return 0;
}
/*
* Gets player input and converts it into move dirrection
*/
Direction InputManager::GetInputDirection(int ch) const
{
	switch (ch)
	{
		case INPUT_UP:
		case INPUT_UP_C:
		case INPUT_UP_R:
		case INPUT_UP_RC: return Direction::UP;

		case INPUT_DOWN:
		case INPUT_DOWN_C: 
		case INPUT_DOWN_R: 
		case INPUT_DOWN_RC:
		case INPUT_DOWN_U: return Direction::DOWN;

		case INPUT_LEFT: 
		case INPUT_LEFT_C: 
		case INPUT_LEFT_R: 
		case INPUT_LEFT_RC: return Direction::LEFT;

		case INPUT_RIGHT:
		case INPUT_RIGHT_C:
		case INPUT_RIGHT_R: 
		case INPUT_RIGHT_RC: return Direction::RIGHT;
	
		default: return Direction::None;
	}
}
