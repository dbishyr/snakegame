#pragma once

/*
* Class that stores players statistic
*/
class StatWidget
{
public:

	//Gets and sets for working with private values
	int GetScore() const { return Score; }
	int GetSpeed() const { return Speed; }
	
	void SetScore(int score) { Score = score; }
	void SetSpeed(int speed) { Speed = speed; }

private:

	//Stored stats
	int Score = 0;
	int Speed = 1;

};

