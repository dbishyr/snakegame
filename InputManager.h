#pragma once

#include"ITickable.h"
#include"Snake.h"

/*
* Class that responds for all input actions
*/
class InputManager : public ITickable
{
public:

	//Called every tick
	void Tick() override;

	//Sets snake member class
	void SetPlayerSnake(Snake* snake) { m_PlayerSnake = snake; }

	//Gets player input
	int GetInput() const;

	Snake* GetSnake() const { return m_PlayerSnake; }

	//Gets input and converts it to direction 
	Direction GetInputDirection(int ch) const;

private:

	Snake* m_PlayerSnake = nullptr;
};

