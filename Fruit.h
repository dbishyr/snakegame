#pragma once
/*
*Class that stires information about pickups, its position and weight
*/
class Fruit
{
public:

	//Gets to retrun private values
	int GetX() const { return PosX; }
	int GetY() const { return PosY; }
	int GetScoreWeight() const { return ScoreWeight; }

	//Sets to set fruit position
	void SetX(int x) { PosX = x; }
	void SetY(int y) { PosY = y; }

private:
	//Position of the fruit
	int PosX = 0;
	int PosY = 0;

	//Number of points you get by eating the fruit
	const int ScoreWeight = 10;
};

