#include "Ticker.h"
#include "Renderer.h"
#include "InputManager.h"
#include "GameController.h"

#include<conio.h>
#include<iostream>

int main()
{
	//Initializing our game components
	Ticker ticker;
	Snake* snake = new Snake;
	Renderer* renderer = new Renderer;
	InputManager* inputManager = new InputManager;
	GameController* gameController = new GameController;

	//Setting member classes
	renderer->SetController(gameController);
	gameController->SetInputManager(inputManager);
	inputManager->SetPlayerSnake(snake);

	//Adding tickable classes to ticker
	ticker.AddTickable(renderer);
	ticker.AddTickable(inputManager);
	ticker.AddTickable(gameController);
	
	//Drawing the main menu
	renderer->DrawMainMenu();
	if (!_kbhit())
	{
		system("pause");
	}
	system("CLS");

	//Starting our game cycle
	while (!gameController->PendingExit())
	{
		//Increasing game speed if needed
		ticker.SetTickDelay(gameController->GetSpeedMod());

		//Calling Tick() for every tickable class
		ticker.Tick();
	}
}

